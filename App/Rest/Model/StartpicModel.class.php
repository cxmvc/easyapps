<?php
namespace Rest\Model;
use Think\Model;
class StartpicModel extends Model
{
	public function getAll($map='',$order = 'id desc')
    {
        $volist = $this->where($map)->order($order)->select();
        $vvlist = array();
        foreach ($volist as $k) {
            if (method_exists($this, 'ckvo')) {
                $vvlist[] = $this->ckvo($k);
            }else{
                $vvlist[] = $k;
            }
        }
        return $vvlist;
    }
}