<?php
class InfoService extends Model
{
    public function _list_($map = array() ,$page = 1,$limit = 16, $orderby = ' id desc'){
        $list['count'] = $this->where($map)->count('id');
        $list['page'] = $page;
        $list['map'] = $map;
        $list['limit'] = $limit;
        $pagecount = ceil($list['count'] / $list['limit']);
        if ($pagecount < 1) $pagecount =1;
        $list['pagecount'] = $pagecount;
        $vlist = $this->where($map)->page($page,$limit)->order("$orderby ")->select();

        $vvlist = array();
        foreach ($vlist as $k) {
            if (method_exists($this, 'ckvo')) {
                $vvlist[] = $this->ckvo($k);
            }else{
                $vvlist[] = $k;
            }
        }
        $list['volist'] = $vvlist;
        return $list;
    }
}
