<?php


function numberDir($num = 0) {
    $num = sprintf("%09d", $num);
    $dir1 = substr($num, 0, 3);
    $dir2 = substr($num, 3, 2);
    $dir3 = substr($num, 5, 2);
    return $dir1.'/'.$dir2.'/'.$dir3.'/';
}

function bbcode($bbtext) {
    $bbtags = array(
		'[h1]' => '<h1>','[/h1]' => '</h1>',
		'[h2]' => '<h2>','[/h2]' => '</h2>',
		'[h3]' => '<h3>','[/h3]' => '</h3>',

		'[table]' => '<table>','[/table]' => '</table>',
		'[tr]' => '<tr>','[/tr]' => '</tr>',
		'[td]' => '<td>','[/td]' => '</td>',

		'[p]' => '<p>','[/p]' => '</p>',
		'[left]' => '<div style="text-align:left;">','[/left]' => '</div>',
		'[right]' => '<div style="text-align:right;">','[/right]' => '</div>',
		'[center]' => '<div style="text-align:center;">','[/center]' => '</div>',
		'[justify]' => '<div style="text-align:justify;">','[/justify]' => '</div>',

		'[b]' => '<b>','[/b]' => '</b>',
		'[i]' => '<i>','[/i]' => '</i>',
		'[u]' => '<u>','[/u]' => '</u>',
		'[ol]' => '<ol>','[/ol]' => '</ol>',
		'[ul]' => '<ul>','[/ul]' => '</ul>',
		'[li]' => '<li>','[/li]' => '</li>',
		'[br]' => '<br>',
		'[pre]' => '<pre>','[/pre]' => '</pre>',	     
		'[/class]' => '</div>',
	);

	$bbextended = array(
		//"/\n[\s| ]*\r/"=>"<br>", 
		"/\[url](.*?)\[\/url]/i" => "<a href=\"http://$1\" title=\"$1\">$1</a>",
		"/\[url=(.*?)\](.*?)\[\/url\]/i" => "<a href=\"$1\" title=\"$2\">$2</a>",
		"/\[img\]([^[]*)\[\/img\]/i" => "<img src=\"$1\" alt=\" \" />",
		"/\[img=(.*?)\](.*?)\[\/img\]/i" =>"<img src=\"$1\" alt=\"$2\"  title=\"$2\" />",
		"/\[color=(.*?)\](.*?)\[\/color\]/i" => "<font color=\"$1\" >$2</font>",
		"/\[class=(.*?)\]/" => "<div class=\"$1\">",
		//"/\[img=(.*?)\](.*?)\[\/img\]/i" => "<img src=\"$1\" class=\"$2\" />",
	);

	$bbtext = str_ireplace(array_keys($bbtags), array_values($bbtags), $bbtext);
	foreach($bbextended as $match=>$replacement){
		$bbtext = preg_replace($match, $replacement, $bbtext);
	}
	return $bbtext;
}

function filterueditor( $value ) {
    $str = str_replace( "\n", "", $value );
    $str = str_replace( "\r", "", $str );
    $str = addcslashes($str,'/');
    return $str;
}


function getRandSrect() 
{
    $str = substr(md5(time()), 0, 16);
    return $str;
}

function randCode($length = 5, $type = 1) {
    $arr = array(1 => "0123456789", 2 => "abcdefghijklmnopqrstuvwxyz", 3 => "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 4 => "~@#$%^&*(){}[]|");
    if ($type == 0) {
        array_pop($arr);
        $string = implode("", $arr);
    } elseif ($type == "-1") {
        $string = implode("", $arr);
    } else {
        $string = $arr[$type];
    }
    $count = strlen($string) - 1;
    $code = '';
    for ($i = 0; $i < $length; $i++) {
        $code .= $string[rand(0, $count)];
    }
    return $code;
}

function cut( $Str, $Length,$sss = '' ) {
	//$Str为截取字符串，$Length为需要截取的长度
	global $s;
	$i = 0;
	$l = 0;
	$ll = strlen( $Str );
	$s = $Str;
	$f = true;
	//if(isset($Str{$i}))
	while ( $i <= $ll ) {
	if ( ord( $Str{$i} ) < 0x80 ) {
	$l++; $i++;
	} else if ( ord( $Str{$i} ) < 0xe0 ) {
	$l++; $i += 2;
	} else if ( ord( $Str{$i} ) < 0xf0 ) {
	$l += 2; $i += 3;
	} else if ( ord( $Str{$i} ) < 0xf8 ) {
	$l += 1; $i += 4;
	} else if ( ord( $Str{$i} ) < 0xfc ) {
	$l += 1; $i += 5;
	} else if ( ord( $Str{$i} ) < 0xfe ) {
	$l += 1; $i += 6;
	}

	if ( ( $l >= $Length - 1 ) && $f ) {
	$s = substr( $Str, 0, $i );
	$f = false;
	}

	if ( ( $l > $Length ) && ( $i < $ll ) ) {
	// $s = $s . '...'; break; //如果进行了截取，字符串末尾加省略符号“...”
	$s = $s . $sss; break; //如果进行了截取，字符串末尾加省略符号“...”
	}
	}
	return $s;
}
?>